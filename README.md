# Base Symfony Development Container

## A docker-based container setup for developing symfony apps.

## Overview

This project allows for a quick and easy setup of a symfony/skeleton based web project. It was decided to use the base skeleton to allow maximum flexibility for the developer.

## Technologies Used.

- PHP 7.4
- Apache 2.4
- Composer 2
- Symfony 5

## Getting Started

- Copy this project to your development box and enter the directory.
- Set the CONTAINER_NAME and HOST_PORT variables in the root level .env file
- Run `docker-compose up -d --build` to fire up the container.
- Run `docker exec -i -u 1000 <CONTAINER_NAME> composer install` to set up Symfony in the container. 
    - Use the actual name of your container in place of <CONTAINER_NAME>
- Begin developing your app in the `development` directory.

> The root level`.env` file only contains the local port and container name information. This is totally safe to commit. The `.env` file located in the development directory is also committable by default per [symfony philosophy](https://symfony.com/doc/current/best_practices.html#use-environment-variables-for-infrastructure-configuration). Obviously do not put anything important in these files.

## Development

Most development will take place using your ide inside the `development` directory. You will need to work from the command-line inside the container to add dependancies using composer. From a terminal run:

```
docker exec -it -u 1000 <CONTAINER_NAME> bash
```

to enter the container. All the regular composer commands should just work here. Additionally the `/var/www/html/bin` directory has been added to the `PATH` so you should be able to use console commands directly.

> Use the actual name of your container in place of <CONTAINER_NAME> in the above command.

## VS Code
If your development tool of choice is VisualStudio Code (or one of its telemetry-free bretheren) the Symfony coding standards may be enforced by adding the [Symfony for VSCode](https://marketplace.visualstudio.com/items?itemName=TheNouillet.symfony-vscode) extention to the IDE.
