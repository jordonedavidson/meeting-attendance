FROM php:7.4.7-apache-buster

RUN apt-get update && apt-get install -y libmemcached-dev zlib1g-dev \
    && pecl install memcached-3.1.5 \
    && docker-php-ext-enable memcached

RUN set -eux; \
    savedAptMark="$(apt-mark showmanual)"; \
    apt-get update \
    && apt-get -y install --no-install-recommends \
    libfreetype6-dev \
    libjpeg-dev \
    libpng-dev \
    libpq-dev \
    libwebp-dev \
    libzip-dev \
    ; \
    \
    docker-php-ext-configure gd \
    --with-freetype \
    --with-jpeg \
    ; \
    \
    docker-php-ext-install -j "$(nproc)" \
    gd \
    opcache \
    pdo_mysql \
    pdo_pgsql \
    zip \
    ; \
    # reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
    apt-mark auto '.*' > /dev/null; \
    apt-mark manual $savedAptMark; \
    ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
    | awk '/=>/ { print $3 }' \
    | sort -u \
    | xargs -r dpkg-query -S \
    | cut -d: -f1 \
    | sort -u \
    | xargs -rt apt-mark manual; \
    \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
    rm -rf /var/lib/apt/lists/*

# Add zip, unzip, and git here because the above command seems to remove them
RUN apt-get update && apt-get install -y --no-install-recommends zip unzip git;

# Add composer
COPY --from=composer:2.0 /usr/bin/composer /usr/bin/composer

# For symphony command tool to work
ENV PATH="/var/www/html/bin:/var/www/html/vendor/bin:$PATH"

# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
    echo 'opcache.memory_consumption=128'; \
    echo 'opcache.interned_strings_buffer=8'; \
    echo 'opcache.max_accelerated_files=4000'; \
    echo 'opcache.revalidate_freq=60'; \
    echo 'opcache.fast_shutdown=1'; \
    } > /usr/local/etc/php/conf.d/opcache-recommended.ini

# extend the php timeout
RUN { \
    echo 'max_execution_time=0'; \
    } > /usr/local/etc/php/conf.d/extend-timeout.ini

# Apache section

# enable rewrite
RUN a2enmod rewrite;

# Bring in the custom apache conf file
COPY ./000-default.conf /etc/apache2/sites-enabled

WORKDIR /var/www/html
